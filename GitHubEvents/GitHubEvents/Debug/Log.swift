//
//  Log.swift
//  GitHubEvents
//
//  Created by Marzena Komorowska on 21/05/2019.
//  Copyright © 2019 Marzena Komorowska. All rights reserved.
//

public class Log {

    public static var isEnabled = true

    private enum Level {
        case info
        case warning
        case error
    }

    public static func info(_ source: @autoclosure () -> Any, _ msg: @autoclosure () -> String) {
        if Log.isEnabled {
            Log.trace(level: .info, source: source(), msg: msg())
        }
    }

    public static func warning(_ source: @autoclosure () -> Any, _ msg: @autoclosure () -> String) {
        if Log.isEnabled {
            Log.trace(level: .warning, source: source(), msg: msg())
        }
    }

    public static func error(_ source: @autoclosure () -> Any, _ msg: @autoclosure () -> String) {
        if Log.isEnabled {
            Log.trace(level: .error, source: source(), msg: msg())
        }
    }

    public static func error(_ source: @autoclosure () -> Any, _ error: @autoclosure () -> Error) {
        if Log.isEnabled {
            Log.trace(level: .error, source: source(), msg: String(describing: error()))
        }
    }

    public static func traceDeinit(_ source: @autoclosure () -> Any) {
        if Log.isEnabled {
            Log.trace(level: .info, source: source(), msg: "Deinit")
        }
    }

    private static func trace(level: Level, source: Any, msg: String) {
        print("\(String(describing: level).prefix(3).uppercased()) \(type(of: source)): \(msg)")
    }
}
