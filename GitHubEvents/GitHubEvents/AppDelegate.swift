//
//  AppDelegate.swift
//  GitHubEvents
//
//  Created by Marzena Komorowska on 21/05/2019.
//  Copyright © 2019 Marzena Komorowska. All rights reserved.
//

import UIKit
import RxSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var navigator: Navigator!

    var disposeBag = DisposeBag()
    var isDone = false

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        let window = UIWindow(frame: UIScreen.main.bounds)
        self.navigator = Navigator()
        window.rootViewController = navigator.topViewController
        window.makeKeyAndVisible()

        self.window = window

        return true
    }
}
