//
//  DataProvider.swift
//  GitHubEvents
//
//  Created by Marzena Komorowska on 21/05/2019.
//  Copyright © 2019 Marzena Komorowska. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

public class EventsProvider {
    typealias ClientInfo = GitHubPageRequester<[GitHubEvent]>.ClientInfo

    public let eventsDataSource: EventsDataSource

    public var hasMorePages: Bool {
        return self.pageRequester.hasMorePages
    }

    private var eventsUpdater: EventsUpdater
    private let disposeBag = DisposeBag()

    private let pageRequester: GitHubPageRequester<[GitHubEvent]>

    public init() {
        self.eventsDataSource = EventsDataSource()
        self.eventsUpdater = EventsUpdater(eventsDataSource: self.eventsDataSource)

        self.pageRequester = GitHubPageRequester()
        self.pageRequester.data
            .subscribe(onNext: { [weak self] (result) in
                guard let self = self else { return }

                switch result {
                case .success(let clientInfo, let events):
                    let isPollIntervalChanged = clientInfo.pollInterval != self.eventsUpdater.pollInterval

                    self.eventsDataSource.mergeEvents(events)

                    if isPollIntervalChanged {
                        self.eventsUpdater.startUpdates(at: clientInfo.pollInterval)
                    }
                case .notChanged:
                    return
                }
            }, onError: { [weak self] (error) in
                guard let self = self else { return }

                Log.error(self, error)
            }).disposed(by: self.disposeBag)
    }

    deinit {
        Log.traceDeinit(self)
    }

    @discardableResult
    public func loadNextPage() -> Bool {
        guard
            !self.pageRequester.isLoading,
            self.pageRequester.hasMorePages else {
                return false
        }

        return self.pageRequester.loadNextPage()
    }
}
