//
//  NetworkError.swift
//  GitHubEvents
//
//  Created by Marzena Komorowska on 22/05/2019.
//  Copyright © 2019 Marzena Komorowska. All rights reserved.
//

public enum DataError: Error {
    case networkError(Error)
    case parsingError(Error?)
    case invalidHttpResponse(Int)
    case rateLimitExceeded
    case unexpectedHttpResponse
}
