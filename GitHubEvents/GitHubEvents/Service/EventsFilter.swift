//
//  GitHubEventsFilter.swift
//  GitHubEvents
//
//  Created by Marzena Komorowska on 23/05/2019.
//  Copyright © 2019 Marzena Komorowska. All rights reserved.
//

import Foundation
import RxSwift

public class EventsFilter {

    public subscript(_ type: GitHubEventType) -> BehaviorSubject<Bool>? {
        return eventTypes[type]
    }

    public var selectionChanged: Observable<[GitHubEventType]> {
        return self.selectionChangedSubject
    }

    public var selectedTypes: [GitHubEventType] {
        return self.eventTypes
            .filter({ (_, value) -> Bool in
                return (try? value.value()) ?? false
            })
            .map({ (key, _) -> GitHubEventType in
                return key
            })
            .sorted(by: { (lhs, rhs) -> Bool in
                return lhs.rawValue < rhs.rawValue
            })
    }

    private struct Config {
        static let filterUserDefaultsKey = "filter"
    }

    private var eventTypes = [GitHubEventType: BehaviorSubject<Bool>]()
    private let selectionChangedSubject: BehaviorSubject<[GitHubEventType]>
    private var disposeBag = DisposeBag()

    init() {
        let selectedTypes = EventsFilter.load()
        self.selectionChangedSubject = BehaviorSubject(value: selectedTypes)

        GitHubEventType.allCases.forEach { (eventType) in
            let eventTypeSubject = BehaviorSubject(value: selectedTypes.contains(eventType))
            self.eventTypes[eventType] = eventTypeSubject

            eventTypeSubject
                .subscribe(onNext: { [weak self] (_) in
                    guard let self = self else { return }

                    self.save(selectedTypes: self.selectedTypes)
                    self.selectionChangedSubject.onNext(self.selectedTypes)
                })
                .disposed(by: self.disposeBag)
        }
    }

    deinit {
        Log.traceDeinit(self)
    }

    private func save(selectedTypes: [GitHubEventType]) {
        let selectedTypesNames = selectedTypes.map({$0.rawValue})
        UserDefaults.standard.setValue(selectedTypesNames, forKeyPath: Config.filterUserDefaultsKey)
    }

    private static func load() -> [GitHubEventType] {
        if let selectedTypesNames = UserDefaults.standard.array(forKey: Config.filterUserDefaultsKey) as? [String] {
            Log.info(self, "Loaded saved filter. \(selectedTypesNames.count) selected")
            return selectedTypesNames.map({GitHubEventType(rawValue: $0)!})
        } else {
            return GitHubEventType.allCases
        }
    }
}
