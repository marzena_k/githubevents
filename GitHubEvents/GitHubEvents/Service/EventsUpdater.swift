//
//  DataUpdater.swift
//  GitHubEvents
//
//  Created by Marzena Komorowska on 24/05/2019.
//  Copyright © 2019 Marzena Komorowska. All rights reserved.
//

import RxSwift
import RxCocoa

class EventsUpdater {
    typealias ClientInfo = GitHubPageRequester<[GitHubEvent]>.ClientInfo

    private(set) public var pollInterval: DispatchTimeInterval = .seconds(Int.max)

    private let eventsDataSource: EventsDataSource
    private let pageRequester: GitHubPageRequester<[GitHubEvent]>
    private var updatesTimer: Disposable?
    private var eTag: String?
    private let disposeBag = DisposeBag()

    public init(eventsDataSource: EventsDataSource) {
        self.eventsDataSource = eventsDataSource

        self.pageRequester = GitHubPageRequester(isResetable: true)
        self.pageRequester.data
            .subscribe(onNext: { [weak self] (result) in
                guard let self = self else { return }

                switch result {
                case .success(let clientInfo, let events):
                    let isPollIntervalChanged = clientInfo.pollInterval != self.pollInterval
                    let hasAllNewItems = !self.eventsDataSource.mergeEvents(events)

                    if self.pageRequester.currentPage == 1 {
                        Log.info(self, "Save eTag")
                        self.eTag = clientInfo.etag
                    }

                    // all new events mean that we may need to fetch more than one page
                    // during updating
                    if hasAllNewItems && self.pageRequester.hasMorePages {
                        self.pageRequester.loadNextPage()
                    } else if isPollIntervalChanged {
                        self.startUpdates(at: clientInfo.pollInterval)
                    }
                case .notChanged:
                    return
                }
            }, onError: { [weak self] (error) in
                    guard let self = self else { return }

                    Log.error(self, error)
            }).disposed(by: self.disposeBag)
    }

    deinit {
        self.updatesTimer?.dispose()
        Log.traceDeinit(self)
    }

    func startUpdates(at pollInterval: DispatchTimeInterval) {
        if let oldUpdatesTimer = self.updatesTimer {
            oldUpdatesTimer.dispose()
        }

        self.pollInterval = pollInterval

        Log.info(self, "Start updates with interval \(self.pollInterval)")

        self.updatesTimer = Observable<Int>.interval(pollInterval, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] (_) in
                guard let self = self else { return }

                if !self.pageRequester.isLoading {
                    self.pageRequester.reset(etag: self.eTag)
                    self.pageRequester.loadNextPage()
                }
            })
    }
}
