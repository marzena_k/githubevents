//
//  NetworkResponse.swift
//  GitHubEvents
//
//  Created by Marzena Komorowska on 22/05/2019.
//  Copyright © 2019 Marzena Komorowska. All rights reserved.
//
import Foundation

struct GitHubResponse {
    public let url: URL
    public let statusCode: Int
    public let etag: String?
    public let nextUrl: URL?
    public let rateLimitRemaining: Int
    public let pollInterval: Int?
}

extension GitHubResponse {

    static func fromURLResponse(_ urlResponse: HTTPURLResponse) -> GitHubResponse? {
        guard let url = urlResponse.url else {
            Log.warning(self, "URL not provided in URLResponse")
            return nil
        }

        guard let rateLimitRemaining = urlResponse.headerAsInt(for: "X-RateLimit-Remaining") else {
                Log.warning(self, "Missing or invalid X-RateLimit-Remaining header")
                return nil
        }

        let pollInterval = urlResponse.headerAsInt(for: "X-Poll-Interval")
        let etag = urlResponse.allHeaderFields["Etag"] as? String
        let link: HttpLinkHeader?
        if let linkHeader = urlResponse.allHeaderFields["Link"] as? String {
            link = HttpLinkHeader.parse(headerValue: linkHeader)
        } else {
            link = nil
        }

        return GitHubResponse(url: url,
                              statusCode: urlResponse.statusCode,
                              etag: etag,
                              nextUrl: link?["next"],
                              rateLimitRemaining: rateLimitRemaining,
                              pollInterval: pollInterval)
    }
}

private extension HTTPURLResponse {

    func headerAsInt(for key: String) -> Int? {
        if let headerValue = self.allHeaderFields[key] as? String {
            return Int(headerValue)
        } else {
            return nil
        }
    }
}
