//
//  GitHubConfiguration.swift
//  GitHubEvents
//
//  Created by Marzena Komorowska on 22/05/2019.
//  Copyright © 2019 Marzena Komorowska. All rights reserved.
//

struct GitHubConfiguration {

    static let apiUrl = "https://api.github.com"
    static let eventsEndpoint = "events"
}
