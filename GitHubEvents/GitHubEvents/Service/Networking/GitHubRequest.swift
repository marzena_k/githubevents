//
//  NetworkRequest.swift
//  GitHubEvents
//
//  Created by Marzena Komorowska on 22/05/2019.
//  Copyright © 2019 Marzena Komorowska. All rights reserved.
//

import Foundation
import RxSwift

struct GitHubRequest {

    public let url: URL
    public let etag: String?
}

extension GitHubRequest {

    static func forEvents(etag: String? = nil) -> GitHubRequest {
        let url = URL(string: "\(GitHubConfiguration.apiUrl)/\(GitHubConfiguration.eventsEndpoint)?page=1")!
        return GitHubRequest(url: url, etag: etag)
    }
}

extension GitHubRequest {

    var urlRequest: URLRequest {
        let request = NSMutableURLRequest(url: self.url)

        if let etag = self.etag {
            request.setValue(etag, forHTTPHeaderField: "Etag")
        }

        return request as URLRequest
    }
}

extension GitHubRequest {

    func response<DataType>() -> Observable<(GitHubResponse, DataType)> where DataType: Decodable {

        let observable = Observable<(GitHubResponse, DataType)>.create({ (observer) -> Disposable in
            let request = self.urlRequest

            Log.info(self, "Get \(self.url)")

            return URLSession.shared.rx.response(request: request)
                .subscribe(onNext: { (response, data) in
                    let statusCode = response.statusCode

                    Log.info(self, "\(statusCode):  \(self.url)")

                    guard let gitHubResponse = GitHubResponse.fromURLResponse(response) else {
                        observer.onError(DataError.unexpectedHttpResponse)
                        return
                    }

                    switch statusCode {
                    case 200..<300:
                        do {
                            let parsedObject: DataType = try data.parse()

                            Log.info(self, "200: \(self.url)")

                            observer.onNext((gitHubResponse, parsedObject))
                        } catch {
                            Log.error(self, error)
                            Log.info(self, String(data: data, encoding: .utf8)!)
                            observer.onError(DataError.parsingError(error))
                        }
                    case 403:
                        if gitHubResponse.rateLimitRemaining == 0 {
                            observer.onError(DataError.rateLimitExceeded)
                        } else {
                            fallthrough
                        }
                    default:
                        observer.onError(DataError.invalidHttpResponse(statusCode))
                    }
                }, onError: { (error) in
                    observer.onError(error)
                }, onCompleted: {
                    observer.onCompleted()
                })
        })

        return observable
    }
}
