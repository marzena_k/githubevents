//
//  GitHubPageRequester.swift
//  GitHubEvents
//
//  Created by Marzena Komorowska on 22/05/2019.
//  Copyright © 2019 Marzena Komorowska. All rights reserved.
//

import Foundation
import RxSwift

struct Config {
    static let rateLimitExceededSecondsDelay = DispatchTimeInterval.seconds(5 * 60)
}

class GitHubPageRequester<DataType> where DataType: Decodable {

    struct ClientInfo {
        let pollInterval: DispatchTimeInterval
        let etag: String?
    }

    enum Result<DataType> {
        case success(ClientInfo, DataType)
        case notChanged
    }

    var data: Observable<Result<DataType>> {
        return self.dataSubject
    }

    var hasMorePages: Bool {
        return self.nextRequest != nil
    }

    private(set) var currentPage = 1
    private(set) var isLoading = false

    private var dataSubject = PublishSubject<Result<DataType>>()
    private var nextRequest: GitHubRequest?
    private var etag: String?
    private var isResetable: Bool
    private var disposeBag = DisposeBag()

    init(etag: String? = nil, isResetable: Bool = false) {
        self.isResetable = isResetable

        self.reset(etag: etag)
    }

    deinit {
        Log.traceDeinit(self)
    }

    func reset(etag: String? = nil) {
        self.etag = etag
        self.nextRequest = GitHubRequest.forEvents(etag: etag)
    }

    @discardableResult
    func loadNextPage() -> Bool {
        guard let request = self.nextRequest, !self.isLoading else {
                return false
        }

        self.isLoading = true

        request.response()
            .retryWhen({ [weak self] (errors) -> Observable<Int> in
                guard let self = self else { return .empty() }

                return errors.flatMapLatest({ (error) -> Observable<Int> in
                    switch error {
                    case DataError.rateLimitExceeded:
                        Log.warning(self, "Rate limit exceeded. Will retry in \(Config.rateLimitExceededSecondsDelay)")

                        return Observable<Int>.interval(Config.rateLimitExceededSecondsDelay,
                                                        scheduler: MainScheduler.instance)
                    default:
                        throw error
                    }
                })
            })
            .subscribe(onNext: { [weak self] (response, data: DataType) in
                guard let self = self else { return }

                let clientInfo = ClientInfo(response: response)

                self.isLoading = false

                Log.info(self, "Remaining rate limit \(response.rateLimitRemaining)")

                if let nextUrl =  response.nextUrl {
                    self.nextRequest = GitHubRequest(url: nextUrl, etag: self.etag)
                    self.currentPage += 1
                } else {
                    self.nextRequest = nil

                    if !self.isResetable {
                        self.dataSubject.onCompleted()
                    }
                }

                self.dataSubject.onNext(.success(clientInfo, data))

            }, onError: { [weak self] (error) in
                guard let self = self else { return }

                self.isLoading = false

                switch error {
                case DataError.invalidHttpResponse(304):
                    self.dataSubject.onNext(.notChanged)
                default:
                    self.dataSubject.onError(error)
                }
            }).disposed(by: self.disposeBag)

        return true
    }
}

private extension GitHubPageRequester.ClientInfo {

    init(response: GitHubResponse) {
        self.pollInterval = .seconds(response.pollInterval ?? 0)
        self.etag = response.etag
    }
}
