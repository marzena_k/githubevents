//
//  LinkHeaderParser.swift
//  GitHubEvents
//
//  Created by Marzena Komorowska on 22/05/2019.
//  Copyright © 2019 Marzena Komorowska. All rights reserved.
//
import Foundation

struct HttpLinkHeader {

    var keys: [String] {
        return [String](self.links.keys)
    }

    subscript(_ relationship: String) -> URL? {
        return self.links[relationship]
    }

    private let links: [String: URL]

    private init(links: [String: URL]) {
        self.links = links
    }
}

extension HttpLinkHeader {

    static func parse(headerValue: String) -> HttpLinkHeader? {
        var links = [String: URL]()
        var isValid = true

        // Match something like: <url1>; rel="next", <url2>; rel="last"
        let pattern = "\\s*<([^>]+)>\\s*;\\s*rel=\"?(\\w+)\"?\\s*,?\\s*"

        do {
            try headerValue.match(pattern: pattern) { (captures) in
                guard captures.count == 3 else {
                    isValid = false
                    return
                }

                guard let url = URL(string: captures[1]) else {
                    isValid = false
                    return
                }

                links[captures[2]] = url
            }
        } catch {
            Log.error(self, error)
            return nil
        }

        return isValid ? HttpLinkHeader(links: links) : nil
    }
}
