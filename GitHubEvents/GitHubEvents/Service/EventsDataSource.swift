//
//  Events.swift
//  GitHubEvents
//
//  Created by Marzena Komorowska on 23/05/2019.
//  Copyright © 2019 Marzena Komorowska. All rights reserved.
//

import RxSwift
import RxCocoa

public class EventsDataSource {

    public let filter: EventsFilter

    public var events: Observable<[GitHubEvent]> {
        return eventsSubject
    }

    private struct Config {
        static let eventsLimit = 500
        static let filterUserDefaultsKey = "filter"
    }

    private let eventsSubject = BehaviorSubject<[GitHubEvent]>(value: [])
    private var loadedEvents = [GitHubEvent]()
    private let disposeBag = DisposeBag()

    public init() {
        self.filter = EventsFilter()

        self.filter.selectionChanged
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] (_) in
                guard let self = self else { return }

                self.filterEvents(self.loadedEvents)
            })
            .disposed(by: self.disposeBag)
    }

    deinit {
        Log.traceDeinit(self)
    }

    @discardableResult
    func mergeEvents(_ events: [GitHubEvent]) -> Bool {
        var oldEvents = self.loadedEvents
        let eventsWithoutDuplicates = events.filter {!oldEvents.contains($0)}
        let duplicatesCount = events.count - eventsWithoutDuplicates.count
        let hasDuplicates = duplicatesCount > 0

        Log.info(self, "New: \(eventsWithoutDuplicates.count) duplicates: \(duplicatesCount)")

        oldEvents.append(contentsOf: eventsWithoutDuplicates)
        oldEvents.sort(by: >)

        self.loadedEvents = [GitHubEvent](oldEvents.prefix(Config.eventsLimit))
        self.filterEvents(self.loadedEvents)

        return hasDuplicates
    }

    private func filterEvents(_ events: [GitHubEvent]) {
        let newEvents = events.filter { (event) -> Bool in
            return self.filter.selectedTypes.contains(event.type)
        }

        Log.info(self, "Matched \(newEvents.count) of \(self.loadedEvents.count)")

        self.eventsSubject.onNext(newEvents)
    }
}
