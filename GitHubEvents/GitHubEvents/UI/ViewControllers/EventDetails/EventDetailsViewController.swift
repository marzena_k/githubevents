//
//  EventDetailsViewController.swift
//  GitHubEvents
//
//  Created by Marzena Komorowska on 23/05/2019.
//  Copyright © 2019 Marzena Komorowska. All rights reserved.
//

import UIKit

class EventDetailsViewController: UIViewController {

    private let event: GitHubEvent

    @IBOutlet private var eventView: EventView!
    @IBOutlet private var payloadContainer: UIView!

    init(event: GitHubEvent) {
        self.event = event

        super.init(nibName: String(describing: EventDetailsViewController.self), bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    deinit {
        Log.traceDeinit(self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = self.event.type.color
        self.eventView.displayEvent(self.event)
        self.addPayloadView()
    }

    private func addPayloadView() {
        if  let payload = self.event.payload,
            let payloadView = self.event.type.createViewForPayload(payload) {

            self.payloadContainer.addSubview(payloadView)

            payloadView.topAnchor.constraint(equalTo: self.eventView.bottomAnchor).isActive = true
            payloadView.pinToSuperview(anchors: [.leading, .trailing, .bottom])
        }
    }
}
