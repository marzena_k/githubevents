//
//  EventFilterViewController.swift
//  GitHubEvents
//
//  Created by Marzena Komorowska on 23/05/2019.
//  Copyright © 2019 Marzena Komorowska. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

public  class EventFilterViewController: UIViewController {

    private let dataSource: EventsFilter
    private let disposeBag = DisposeBag()

    @IBOutlet private var tableView: UITableView!

    init(dataSource: EventsFilter) {
        self.dataSource = dataSource

        super.init(nibName: String(describing: EventFilterViewController.self), bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    deinit {
        Log.traceDeinit(self)
    }

    public override func viewDidLoad() {
        super.viewDidLoad()

        self.setupTableViewDataSource()
    }

    private func setupTableViewDataSource() {
        let cellIdentifier = String(describing: FilterCell.self)

        self.tableView.register(UINib(nibName: cellIdentifier, bundle: nil),
                                forCellReuseIdentifier: cellIdentifier)

        let cellProvider: TableViewDataSource<GitHubEventType>.CellProvider
        cellProvider = { [weak self] (tableView, indexPath, eventType) in

            guard let self = self else {
                return UITableViewCell()
            }

            guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? FilterCell else {
                fatalError("Invalid or not registered cell")
            }

            if let subject = self.dataSource[eventType] {
                cell.displayEventType(eventType)
                cell.bind(to: subject)
            }

            return cell
        }

        let dataSource = TableViewDataSource<GitHubEventType>(cellProvider: cellProvider)

        Observable.just(GitHubEventType.allCases)
            .bind(to: self.tableView.rx.items(dataSource: dataSource))
            .disposed(by: self.disposeBag)
    }
}
