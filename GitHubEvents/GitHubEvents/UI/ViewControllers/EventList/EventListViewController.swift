//
//  EventListViewController.swift
//  GitHubEvents
//
//  Created by Marzena Komorowska on 21/05/2019.
//  Copyright © 2019 Marzena Komorowska. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

public class EventListViewController: UIViewController {
    public typealias EventSelectedBlock = (GitHubEvent) -> Void

    public var onEventSelected: Observable<GitHubEvent> {
        self.loadViewIfNeeded()

        return self.tableView.rx.itemSelected.map({ [weak self] (indexPath) -> GitHubEvent in
            guard let self = self else { preconditionFailure() }

            let event = self.dataSource[indexPath]
            return event
        })
    }

    private struct Config {
        static let preloadedPageCount = CGFloat(2.0)
    }

    private let eventsProvider: EventsProvider
    private let disposeBag = DisposeBag()
    private var dataSource: TableViewDataSource<GitHubEvent>!

    @IBOutlet private var tableView: UITableView!

    init(eventsProvider: EventsProvider) {
        self.eventsProvider = eventsProvider

        super.init(nibName: String(describing: EventListViewController.self), bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    deinit {
        Log.traceDeinit(self)
    }

    public override func viewDidLoad() {
        super.viewDidLoad()

        self.setupFilterButton()
        self.setupContentOffsetBindings()
        self.setupDataSource()
    }

    private func setupFilterButton() {
        let barButtonItem = UIBarButtonItem(title: "Filter", style: .plain, target: nil, action: nil)

        barButtonItem.rx.tap.subscribe(onNext: { [weak self] () in
            guard let self = self else { return }

            let viewController = EventFilterViewController(dataSource: self.eventsProvider.eventsDataSource.filter)
            viewController.title = "Filter"
            self.navigationController?.pushViewController(viewController, animated: true)
        }).disposed(by: self.disposeBag)

        self.navigationItem.rightBarButtonItem = barButtonItem
    }

    private func setupContentOffsetBindings() {
        self.tableView.rx.contentOffset.asObservable()
            .map({ [weak self] (contentOffset) -> Bool in
                guard let self = self else { return false }

                guard self.eventsProvider.hasMorePages else {
                    return false
                }

                let currentY = contentOffset.y
                let pageHeight = self.tableView.bounds.height
                let contentHeight = self.tableView.contentSize.height
                let needToLoadMore = currentY + Config.preloadedPageCount * pageHeight > contentHeight

                return needToLoadMore
            })
            .subscribe(onNext: { [weak self] (loadMorePages) in
                guard let self = self else { return }

                if loadMorePages {
                    self.eventsProvider.loadNextPage()
                }
            }).disposed(by: self.disposeBag)
    }

    private func setupDataSource() {
        let cellIdentifier = String(describing: EventCell.self)

        self.tableView.register(UINib(nibName: cellIdentifier, bundle: nil),
                                forCellReuseIdentifier: cellIdentifier)

        let cellProvider: TableViewDataSource<GitHubEvent>.CellProvider
        cellProvider = { (tableView, indexPath, event) -> UITableViewCell in
            guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? EventCell else {
                fatalError("Invalid or not registered cell")
            }
            cell.displayEvent(event)

            return cell
        }

        self.dataSource = TableViewDataSource<GitHubEvent>(cellProvider: cellProvider)

        self.eventsProvider.eventsDataSource.events
            .distinctUntilChanged()
            .bind(to: self.tableView.rx.items(dataSource: dataSource))
            .disposed(by: self.disposeBag)
    }
}
