//
//  CommitView.swift
//  GitHubEvents
//
//  Created by Marzena Komorowska on 24/05/2019.
//  Copyright © 2019 Marzena Komorowska. All rights reserved.
//

import UIKit

public class CommitView: ViewFromNib {

    @IBOutlet private var messageLabel: UILabel!
    @IBOutlet private var authorLabel: UILabel!

    public func displayCommit(_ commit: GitHubCommit) {
        self.messageLabel.text = commit.message
        self.authorLabel.text = "\(commit.author.name) (\(commit.author.email))"
    }
}
