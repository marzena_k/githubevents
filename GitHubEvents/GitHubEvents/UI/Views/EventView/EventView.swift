//
//  EventView.swift
//  GitHubEvents
//
//  Created by Marzena Komorowska on 24/05/2019.
//  Copyright © 2019 Marzena Komorowska. All rights reserved.
//

import UIKit

public class EventView: ViewFromNib {

    @IBOutlet private var dateLabel: UILabel!
    @IBOutlet private var repoLabel: UILabel!
    @IBOutlet private var repoImageView: UIImageView!
    @IBOutlet private var actorLoginLabel: UILabel!
    @IBOutlet private var actorImageView: UIImageView!

    public override func awakeFromNib() {
        super.awakeFromNib()

        self.repoImageView.setBorder()
        self.actorImageView.setBorder()
    }

    public func displayEvent(_ event: GitHubEvent) {
        self.dateLabel.text = event.creationDate.displayString
        self.repoLabel.text = event.repository
        self.actorLoginLabel.text = event.actor.login

        if let url = event.avatarUrl {
            self.repoImageView.kf.setImage(with: url)
        }

        if let url = event.actor.avatarUrl {
            self.actorImageView.kf.setImage(with: url)
        }

    }
}
