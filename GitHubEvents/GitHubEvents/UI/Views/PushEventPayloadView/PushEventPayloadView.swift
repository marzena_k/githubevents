//
//  PushEventPayloadView.swift
//  GitHubEvents
//
//  Created by Marzena Komorowska on 24/05/2019.
//  Copyright © 2019 Marzena Komorowska. All rights reserved.
//

import UIKit

public class PushEventPayloadView: ViewFromNib {

    @IBOutlet private var refLabel: UILabel!
    @IBOutlet private var sizeLabel: UILabel!
    @IBOutlet private var commitView: CommitView!

    public func displayPushEventPayload(_ pushEventPayload: GitHubPushEventPayload) {
        refLabel.text = pushEventPayload.ref
        sizeLabel.text = "Number of commits: \(pushEventPayload.size) (distinct: \(pushEventPayload.distinctSize))"

        if let commit = pushEventPayload.commits.first {
            commitView.displayCommit(commit)
        } else {
            commitView.isHidden = true
        }
    }

    public override func configure() {
        self.commitView.setBorder()
    }
}
