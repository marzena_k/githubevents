//
//  ViewFromNib.swift
//  GitHubEvents
//
//  Created by Marzena Komorowska on 24/05/2019.
//  Copyright © 2019 Marzena Komorowska. All rights reserved.
//

import UIKit

@IBDesignable
open class ViewFromNib: UIView {

    public required init?(coder decoder: NSCoder) {
        super.init(coder: decoder)
        self.loadAndConfigure()
    }

    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadAndConfigure()
    }

    init() {
        super.init(frame: CGRect.zero)
        self.loadAndConfigure()
    }

    open func configure() {
    }

    private func loadAndConfigure() {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)

        guard let view = nib.instantiate(withOwner: self, options: nil).first as? UIView else {
            fatalError("UIView not found in xib \(String(describing: type(of: self)))")
        }

        self.backgroundColor = UIColor.clear

        view.translatesAutoresizingMaskIntoConstraints = false

        self.addSubview(view)

        view.pinToSuperview(anchors: .all)

        self.configure()
    }
}
