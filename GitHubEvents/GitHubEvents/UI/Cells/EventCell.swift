//
//  EventCell.swift
//  GitHubEvents
//
//  Created by Marzena Komorowska on 22/05/2019.
//  Copyright © 2019 Marzena Komorowska. All rights reserved.
//

import UIKit
import Kingfisher

class EventCell: UITableViewCell {

    @IBOutlet private var typeLabel: UILabel!
    @IBOutlet private var actorLabel: UILabel!
    @IBOutlet private var repositoryLabel: UILabel!
    @IBOutlet private var dateLabel: UILabel!
    @IBOutlet private var avatarImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()

        self.selectionStyle = .blue
        self.avatarImageView.setBorder()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        self.avatarImageView.image = UIImage(named: "github")
    }

    func displayEvent(_ event: GitHubEvent) {
        self.contentView.backgroundColor = event.type.color

        self.typeLabel.text = event.type.name
        self.actorLabel.text = event.actor.login
        self.repositoryLabel.text = event.repository
        self.dateLabel.text = event.creationDate.displayString

        if let url = event.avatarUrl {
            self.avatarImageView.kf.setImage(with: url)
        }
    }
}
