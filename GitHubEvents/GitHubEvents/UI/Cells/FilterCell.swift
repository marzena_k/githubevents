//
//  FilterCell.swift
//  GitHubEvents
//
//  Created by Marzena Komorowska on 23/05/2019.
//  Copyright © 2019 Marzena Komorowska. All rights reserved.
//

import UIKit
import RxSwift

class FilterCell: UITableViewCell {

    @IBOutlet private var switchControl: UISwitch!
    @IBOutlet private var titleLabel: UILabel!

    private var disposeBag = DisposeBag()

    override func prepareForReuse() {
        self.disposeBag = DisposeBag()
    }

    func displayEventType(_ eventType: GitHubEventType) {
        //self.contentView.backgroundColor = eventType.color
        self.titleLabel.text = eventType.name
    }

    func bind(to subject: BehaviorSubject<Bool>) {
        self.switchControl.isOn = (try? subject.value()) ?? false

        subject
            .bind(to: self.switchControl.rx.isSelected)
            .disposed(by: self.disposeBag)

        self.switchControl.rx.isOn
            .subscribe(subject)
            .disposed(by: self.disposeBag)
    }
}
