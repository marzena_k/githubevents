//
//  Navigator.swift
//  GitHubEvents
//
//  Created by Marzena Komorowska on 24/05/2019.
//  Copyright © 2019 Marzena Komorowska. All rights reserved.
//
import UIKit
import RxSwift

public class Navigator {

    public var topViewController: UIViewController {
        return self.navigationController
    }

    private let navigationController = UINavigationController()
    private let disposeBag = DisposeBag()
    private let eventsProvider = EventsProvider()

    public init() {
        showEvents()
    }

    deinit {
        Log.traceDeinit(self)
    }

    private func showEvents() {
        let viewController = EventListViewController(eventsProvider: self.eventsProvider)
        viewController.title = "Events"

        viewController.onEventSelected.subscribe(onNext: { (event) in
            self.showEventDetails(event)
        }).disposed(by: self.disposeBag)

        let barButtonItem = self.createBarButtonWithTitle("Filter", block: self.showFilter)
        viewController.navigationItem.rightBarButtonItem = barButtonItem

        self.navigationController.pushViewController(viewController, animated: true)
    }

    private func showEventDetails(_ event: GitHubEvent) {
        let viewController = EventDetailsViewController(event: event)
        viewController.title = event.type.name

        self.navigationController.pushViewController(viewController, animated: true)
    }

    private func showFilter() {
        let viewController = EventFilterViewController(dataSource: self.eventsProvider.eventsDataSource.filter)
        viewController.title = "Filter"

        self.navigationController.pushViewController(viewController, animated: true)
    }
}

fileprivate extension Navigator {

    func createBarButtonWithTitle(_ title: String, block: @escaping () -> Void) -> UIBarButtonItem {
        let barButtonItem = UIBarButtonItem(title: title, style: .plain, target: nil, action: nil)

        barButtonItem.rx.tap.subscribe(onNext: {
            block()
        }).disposed(by: self.disposeBag)

        return barButtonItem
    }
}
