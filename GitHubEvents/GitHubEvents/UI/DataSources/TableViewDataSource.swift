//
//  TableViewDataSource.swift
//  GitHubEvents
//
//  Created by Marzena Komorowska on 23/05/2019.
//  Copyright © 2019 Marzena Komorowska. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

public class TableViewDataSource<DataType: Equatable>: NSObject, UITableViewDataSource, RxTableViewDataSourceType {
    public typealias Element = [DataType]
    public typealias CellProvider =  (UITableView, IndexPath, DataType) -> UITableViewCell
    private typealias ContentOffset = (row: Int, offset: CGFloat)

    public subscript(_ indexPath: IndexPath) -> DataType {
        return self.items[indexPath.row]
    }

    private let cellProvider: CellProvider
    private var items = [DataType]()

    public init(cellProvider: @escaping CellProvider) {
        self.cellProvider = cellProvider
    }

    public func tableView(_ tableView: UITableView, observedEvent: Event<[DataType]>) {
        switch observedEvent {
        case .next(let newItems):
            self.merge(newItems, tableView: tableView)
            return
        case .error(let error):
            Log.error(self, error)
            return
        case .completed:
            return
        }
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = self.items[indexPath.row]

        return self.cellProvider(tableView, indexPath, item)
    }

    private func merge(_ newItems: [DataType], tableView: UITableView) {
        var contentOffset = saveContentOffset(in: tableView)

        if contentOffset.row == 0 && contentOffset.offset == 0 {
            let deletedIndexes = newItems.removedIndexPaths(from: self.items)
            let insertedIndexes = self.items.removedIndexPaths(from: newItems)

            self.items = newItems

            tableView.performBatchUpdates({
                tableView.deleteRows(at: deletedIndexes, with: .automatic)
                tableView.insertRows(at: insertedIndexes, with: .automatic)
            }, completion: nil)
        } else {
            contentOffset = self.modifyContentOffset(contentOffset, for: newItems)

            self.items = newItems
            tableView.reloadData()
            self.restoreContentOffset(contentOffset, in: tableView)
        }
    }

    private func saveContentOffset(in tableView: UITableView) -> ContentOffset {
        let topRow = tableView.indexPathForRow(at: tableView.contentOffset)?.row ?? 0

        var cellOffset = CGFloat(0.0)

        if let cell = tableView.cellForRow(at: IndexPath(row: topRow, section: 0)) {
            cellOffset = cell.frame.minY  -  tableView.contentOffset.y
        }

        return (row: topRow, offset: cellOffset)
    }

    private func restoreContentOffset(_ contentOffset: ContentOffset, in tableView: UITableView) {
        if contentOffset.row < self.items.count {
            tableView.scrollToRow(at: IndexPath(row: contentOffset.row, section: 0), at: .top, animated: false)

            // adjust to cell offset and ensure it's within content size
            let newY = min(tableView.contentOffset.y - contentOffset.offset,
                           tableView.contentSize.height - tableView.bounds.height)

            tableView.contentOffset = CGPoint(x: tableView.contentOffset.x, y: newY)
        }
    }

    private func modifyContentOffset(_ contentOffset: ContentOffset, for newItems: [DataType]) -> ContentOffset {
        var newTopRow = contentOffset.row

        if  self.items.count > contentOffset.row,
            let oldItemNewIndex = newItems.firstIndex(of: self.items[contentOffset.row]) {

            newTopRow = oldItemNewIndex
        }

        while newTopRow >= newItems.count && newTopRow > 0 {
            newTopRow -= 1
        }

        return ContentOffset(row: newTopRow, offset: contentOffset.offset)
    }
}

fileprivate extension Array where Element: Equatable {

    func removedIndexPaths(from array: [Element]) -> [IndexPath] {
        let removedElements = array.filter {!self.contains($0)}

        return removedElements.reduce(into: [IndexPath]()) { (result, item) in
            result.append(IndexPath(row: array.firstIndex(of: item)!, section: 0))
        }
    }
}
