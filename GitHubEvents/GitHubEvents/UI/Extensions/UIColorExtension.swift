//
//  UIColorExtension.swift
//  GitHubEvents
//
//  Created by Marzena Komorowska on 22/05/2019.
//  Copyright © 2019 Marzena Komorowska. All rights reserved.
//

import UIKit

public extension UIColor {

    struct Pastel {
        static var beige = UIColor(rgb: 0xF2E2E3)
        static var blue = UIColor(rgb: 0xB2CEFE)
        static var burlyWood = UIColor(rgb: 0xE7BDB3)
        static var lightGreen = UIColor(rgb: 0x9BD3CB)
        static var green = UIColor(rgb: 0xBAED91)
        static var khaki = UIColor(rgb: 0xF0DF93)
        static var lightSteel = UIColor(rgb: 0xECEDED)
        static var orange = UIColor(rgb: 0xF8B88B)
        static var purple = UIColor(rgb: 0xF0E0F7)
        static var red = UIColor(rgb: 0xFEA3AA)
        static var yellow = UIColor(rgb: 0xFAF884)
    }

    convenience init(rgb: Int, alpha: CGFloat = 1.0) {
        let red = CGFloat((rgb & 0xFF0000) >> 16)/255.0
        let green = CGFloat((rgb & 0xFF00) >> 8)/255.0
        let blue = CGFloat(rgb & 0xFF)/255.0

        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
}
