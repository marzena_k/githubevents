//
//  EventTypeColor.swift
//  GitHubEvents
//
//  Created by Marzena Komorowska on 22/05/2019.
//  Copyright © 2019 Marzena Komorowska. All rights reserved.
//
import UIKit

public extension GitHubEventType {

    var color: UIColor {
        switch self {
        case .createEvent: return UIColor.Pastel.lightGreen
        case .deleteEvent: return UIColor.Pastel.red
        case .forkEvent: return UIColor.Pastel.burlyWood
        case .issuesEvent: return UIColor.Pastel.khaki
        case .issueCommentEvent: return UIColor.Pastel.yellow
        case .pullRequestEvent: return UIColor.Pastel.blue
        case .pullRequestReviewCommentEvent: return UIColor.Pastel.purple
        case .pushEvent: return UIColor.Pastel.green
        case .releaseEvent: return UIColor.Pastel.beige
        case .watchEvent: return UIColor.Pastel.lightSteel
        default: return UIColor.white
        }
    }

    func createViewForPayload(_ payload: GitHubEventPayload) -> UIView? {
        switch payload {
        case let payload as GitHubPushEventPayload:
            let view = PushEventPayloadView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.displayPushEventPayload(payload)
            return view
        default: return nil
        }

    }
}
