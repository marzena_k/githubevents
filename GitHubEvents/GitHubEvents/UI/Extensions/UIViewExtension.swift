//
//  UIViewExtension.swift
//  GitHubEvents
//
//  Created by Marzena Komorowska on 22/05/2019.
//  Copyright © 2019 Marzena Komorowska. All rights reserved.
//

import UIKit

public extension UIView {

    struct Border {
        public static let width = CGFloat(0.0)
        public static let color = UIColor.darkGray
        public static let radius  = CGFloat(10.0)
    }

    func setBorder(width: CGFloat = Border.width,
                   color: UIColor = Border.color,
                   cornerRadius: CGFloat = Border.radius) {

        self.layer.cornerRadius = cornerRadius
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = width
        self.layer.masksToBounds = true
    }
}

public extension UIView {

    struct Anchors: OptionSet {
        public let rawValue: Int

        public static let leading = Anchors(rawValue: 1 << 0)
        public static let trailing = Anchors(rawValue: 1 << 1)
        public static let top = Anchors(rawValue: 1 << 2)
        public static let bottom = Anchors(rawValue: 1 << 3)
        public static let all: Anchors = [.leading, .trailing, .top, .bottom]

        public init(rawValue: Int) {
            self.rawValue = rawValue
        }
    }

    func pinToSuperview(anchors: Anchors) {
        if let superview = self.superview {
            if anchors.contains(.leading) {
                self.leadingAnchor.constraint(equalTo: superview.leadingAnchor).isActive = true
            }
            if anchors.contains(.trailing) {
                self.trailingAnchor.constraint(equalTo: superview.trailingAnchor).isActive = true
            }
            if anchors.contains(.top) {
                self.topAnchor.constraint(equalTo: superview.topAnchor).isActive = true
            }
            if anchors.contains(.bottom) {
                self.bottomAnchor.constraint(equalTo: superview.bottomAnchor).isActive = true
            }
        }
    }
}
