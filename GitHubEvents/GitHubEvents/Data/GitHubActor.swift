//
//  GitHubActor.swift
//  GitHubEvents
//
//  Created by Marzena Komorowska on 24/05/2019.
//  Copyright © 2019 Marzena Komorowska. All rights reserved.
//
import Foundation

public struct GitHubActor: Decodable, Equatable {

    enum CodingKeys: String, CodingKey {
        case login
        case avatarUrl = "avatar_url"
    }

    public let login: String
    public let avatarUrl: URL?

    public static func==(lhs: GitHubActor, rhs: GitHubActor) -> Bool {
        return lhs.login == rhs.login
    }
}
