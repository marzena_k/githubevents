//
//  GitHubEventPayload.swift
//  GitHubEvents
//
//  Created by Marzena Komorowska on 24/05/2019.
//  Copyright © 2019 Marzena Komorowska. All rights reserved.
//

public protocol GitHubEventPayload: Decodable {}
