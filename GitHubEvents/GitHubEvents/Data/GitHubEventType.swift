//
//  EventType.swift
//  GitHubEvents
//
//  Created by Marzena Komorowska on 22/05/2019.
//  Copyright © 2019 Marzena Komorowska. All rights reserved.
//

public enum GitHubEventType: String, Decodable, CaseIterable, RawRepresentable {
    case checkRunEvent
    case checkSuiteEvent
    case commitCommentEvent
    case contentReferenceEvent
    case createEvent
    case deleteEvent
    case deployKeyEvent
    case deploymentEvent
    case deploymentStatusEvent
    case downloadEvent
    case followEvent
    case forkEvent
    case forkApplyEvent
    case gitHubAppAuthorizationEvent
    case gistEvent
    case gollumEvent
    case installationEvent
    case installationRepositoriesEvent
    case issueCommentEvent
    case issuesEvent
    case labelEvent
    case marketplacePurchaseEvent
    case memberEvent
    case membershipEvent
    case metaEvent
    case milestoneEvent
    case organizationEvent
    case orgBlockEvent
    case pageBuildEvent
    case projectCardEvent
    case projectColumnEvent
    case projectEvent
    case publicEvent
    case pullRequestEvent
    case pullRequestReviewCommentEvent
    case pushEvent
    case registryPackageEvent
    case releaseEvent
    case repositoryEvent
    case repositoryImportEvent
    case repositoryVulnerabilityAlertEvent
    case securityAdvisoryEvent
    case starEvent
    case statusEvent
    case teamEvent
    case teamAddEvent
    case watchEvent

    var name: String {
        let words = self.rawValue
            .replacingOccurrences(of: "gitHub", with: "github")
            .splitToWords()

        return words
            .prefix(words.count - 1)
            .joined(separator: " ")
            .capitalized
            .replacingOccurrences(of: "Github", with: "GitHub")
    }
}
