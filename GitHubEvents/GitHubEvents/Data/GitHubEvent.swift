//
//  Event.swift
//  GitHubEvents
//
//  Created by Marzena Komorowska on 21/05/2019.
//  Copyright © 2019 Marzena Komorowska. All rights reserved.
//

import Foundation

public struct GitHubEvent: Decodable, Equatable {

    enum CodingKeys: String, CodingKey {
        case identifier = "id"
        case type
        case repo
        case actor
        case creationDate = "created_at"
        case organization = "org"
        case payload
    }

    enum RepoKeys: String, CodingKey {
        case name
    }

    enum OrganizationKey: String, CodingKey {
        case avatar = "avatar_url"
    }

    public let identifier: String
    public let type: GitHubEventType
    public let repository: String
    public let actor: GitHubActor
    public let creationDate: Date
    public let avatarUrl: URL?
    public let payload: GitHubEventPayload?

    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.identifier = try values.decode(String.self, forKey: .identifier)

        guard let type = GitHubEventType(rawValue: try values.decode(String.self, forKey: .type).decapitalize()) else {
            throw DataError.parsingError(nil)
        }

        self.type = type

        guard let date = Date.parseUnixDate(try values.decode(String.self, forKey: .creationDate)) else {
            throw DataError.parsingError(nil)
        }

        self.creationDate = date

        let repo = try values.nestedContainer(keyedBy: RepoKeys.self, forKey: .repo)
        self.repository = try repo.decode(String.self, forKey: .name)

        if let organization = try? values.nestedContainer(keyedBy: OrganizationKey.self, forKey: .organization) {
            self.avatarUrl = try? organization.decode(URL.self, forKey: .avatar)
        } else {
            self.avatarUrl = nil
        }

        self.actor = try values.decode(GitHubActor.self, forKey: .actor)

        self.payload = try self.type.decodePayload(values: values, key: .payload)
    }

    public static func==(lhs: GitHubEvent, rhs: GitHubEvent) -> Bool {
        return lhs.identifier == rhs.identifier
    }

    public static func<(lhs: GitHubEvent, rhs: GitHubEvent) -> Bool {
        return lhs.creationDate < rhs.creationDate
    }

    public static func>(lhs: GitHubEvent, rhs: GitHubEvent) -> Bool {
        return lhs.creationDate > rhs.creationDate
    }
}

private extension GitHubEventType {

    func decodePayload(values: KeyedDecodingContainer<GitHubEvent.CodingKeys>,
                       key: GitHubEvent.CodingKeys) throws -> GitHubEventPayload? {
        switch self {
        case .pushEvent: return try values.decode(GitHubPushEventPayload.self, forKey: key)
        default: return nil
        }
    }
}
