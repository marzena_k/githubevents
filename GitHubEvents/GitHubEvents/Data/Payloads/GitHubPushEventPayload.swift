//
//  GitHubPushEventPayload.swift
//  GitHubEvents
//
//  Created by Marzena Komorowska on 24/05/2019.
//  Copyright © 2019 Marzena Komorowska. All rights reserved.
//

public struct GitHubPushEventPayload: GitHubEventPayload, Decodable {

    enum CodingKeys: String, CodingKey {
        case ref
        case size
        case distinctSize = "distinct_size"
        case commits
    }

    public let ref: String
    public let size: Int
    public let distinctSize: Int
    public let commits: [GitHubCommit]
}

public struct GitHubCommit: Decodable {

    enum CodingKeys: String, CodingKey {
        case sha
        case message
        case author
        case isDistinct = "distinct"
    }

    public let sha: String
    public let message: String
    public let author: GitHubAuthor
    public let isDistinct: Bool
}

public struct GitHubAuthor: Decodable {

    enum CodingKeys: String, CodingKey {
        case name
        case email
    }

    public let name: String
    public let email: String
}
