//
//  StringExtensions.swift
//  GitHubEvents
//
//  Created by Marzena Komorowska on 22/05/2019.
//  Copyright © 2019 Marzena Komorowska. All rights reserved.
//
import Foundation

public extension String {

    var fullRange: NSRange {
        return NSRange(location: 0, length: self.count)
    }

    subscript(_ range: NSRange) -> Substring {
        let start = self.index(self.startIndex, offsetBy: range.lowerBound)
        let end = self.index(self.startIndex, offsetBy: range.upperBound)
        return self[start..<end]
    }

    func match(pattern: String, using block: ([String]) -> Void) throws {
        let regEx = try NSRegularExpression(pattern: pattern, options: [])

        regEx.enumerateMatches(in: self, options: [], range: self.fullRange) { (match, _, stop) in
            guard let match = match else { return }

            var captures = [String]()

            for index in 0..<match.numberOfRanges {
                let range = match.range(at: index)
                captures.append(String(self[range]))
            }

            block(captures)

            stop.pointee = false
        }
    }

    func splitToWords() -> [String] {
        var words = [String]()
        var isValid = true

        try? self.match(pattern: "([A-Z]?[a-z]+)", using: { (captures) in
            guard captures.count == 2 else {
                isValid =  false
                return
            }
            words.append(captures[0])
        })

        return isValid ? words : [self]
    }

    func decapitalize() -> String {
        if let first = self.first {
            if self.startIndex == self.endIndex {
                return String(first)
            } else {
                return first.lowercased() + self.suffix(from: self.index(after: self.startIndex))
            }
        } else {
            return self
        }
    }
}
