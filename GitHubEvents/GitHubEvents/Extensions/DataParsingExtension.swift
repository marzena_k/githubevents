//
//  DataParsingExtension.swift
//  GitHubEvents
//
//  Created by Marzena Komorowska on 22/05/2019.
//  Copyright © 2019 Marzena Komorowska. All rights reserved.
//
import Foundation

public extension Data {

    func parse<DataType>() throws -> DataType where DataType: Decodable {
        let jsonDecoder = JSONDecoder()
        return try jsonDecoder.decode(DataType.self, from: self)
    }
}
