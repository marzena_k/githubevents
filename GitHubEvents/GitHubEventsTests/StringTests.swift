//
//  StringTests.swift
//  GitHubEventsTests
//
//  Created by Marzena Komorowska on 23/05/2019.
//  Copyright © 2019 Marzena Komorowska. All rights reserved.
//

import XCTest
@testable import GitHubEvents

class StringTests: XCTestCase {

    func testSplitToWords() {
        let string = GitHubEventType.issueCommentEvent.rawValue
        let words = string.splitToWords()

        XCTAssertEqual(words, ["issue", "Comment", "Event"])
    }
}
