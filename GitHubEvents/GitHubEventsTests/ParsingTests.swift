//
//  ParsingTests.swift
//  GitHubEventsTests
//
//  Created by Marzena Komorowska on 22/05/2019.
//  Copyright © 2019 Marzena Komorowska. All rights reserved.
//

import XCTest
@testable import GitHubEvents

class ParsingTests: XCTestCase {

    private var json: Data!

    override func setUp() {
        self.json = Bundle(for: type(of: self)).loadDataFromFile(name: "events.json")
    }

    func testParsingEventsData() {

        do {
            let events: [GitHubEvent] = try json.parse()

            Log.info(self, "\(events)")

            let types = events.reduce(into: Set<String>()) { (result, event) in
                _ = result.insert(event.type)
            }

            types.forEach { (type) in
                Log.info(self, "Type: \(type)")
            }

            XCTAssertEqual(events.count, 30)

            self.checkIssueCommentEvent(events.first!)
            self.checkPushEvent(events[2])

        } catch {
            Log.error(self, error)
            XCTFail()
        }
    }

    private func checkIssueCommentEvent(_ event: GitHubEvent) {
        XCTAssertEqual(event.identifier, "9672192610")
        XCTAssertEqual(event.actor.login, "lepture")
        XCTAssertEqual(event.actor.avatarUrl, URL(string: "https://avatars.githubusercontent.com/u/290496?"))
        XCTAssertEqual(event.repository, "uc-cdis/fence")
        XCTAssertEqual(event.type, GitHubEventType.issueCommentEvent)
    }

    private func checkPushEvent(_ event: GitHubEvent) {
        let payload = event.payload as? GitHubPushEventPayload
        XCTAssertNotNil(event.payload)

        let pushEventPayload =  payload!

        XCTAssertEqual(pushEventPayload.ref, "refs/heads/master")
        XCTAssertEqual(pushEventPayload.size, 1)
        XCTAssertEqual(pushEventPayload.distinctSize, 1)

        XCTAssertEqual(pushEventPayload.commits.count, 1)

        let commit = pushEventPayload.commits.first!
        XCTAssertEqual(commit.author.name, "Amaresh")
        XCTAssertEqual(commit.author.email, "amaresh_adak@yahoo.com")
        XCTAssertEqual(commit.message, "update gauge charts")
        XCTAssertEqual(commit.isDistinct, true)
    }
}

extension Bundle {

    func loadDataFromFile(name: String) -> Data {
        let url = self.resourceURL!.appendingPathComponent(name)
        return try! Data(contentsOf: url)
    }
}
