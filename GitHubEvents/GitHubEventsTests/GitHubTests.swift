//
//  GitHubTests.swift
//  GitHubEventsTests
//
//  Created by Marzena Komorowska on 22/05/2019.
//  Copyright © 2019 Marzena Komorowska. All rights reserved.
//

import XCTest
@testable import GitHubEvents

class GitHubTests: XCTestCase {

    func testEventsUrl() {
        let gitHubRequest = GitHubRequest.forEvents()

        XCTAssertEqual(gitHubRequest.url, URL(string: "https://api.github.com/events?page=1"))
        XCTAssertNil(gitHubRequest.etag)
    }

}
