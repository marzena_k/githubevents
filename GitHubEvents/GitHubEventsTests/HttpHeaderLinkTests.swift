//
//  HttpHeaderLinkTests.swift
//  GitHubEventsTests
//
//  Created by Marzena Komorowska on 22/05/2019.
//  Copyright © 2019 Marzena Komorowska. All rights reserved.
//

import XCTest
@testable import GitHubEvents

class HttpHeaderLinkTests: XCTestCase {

    func testLinkHeaderParsingSingle() {
        let link = HttpLinkHeader.parse(headerValue: "<meta.rdf>;rel=meta")

        XCTAssertNotNil(link)
        XCTAssertEqual(link!.keys.count, 1)
        XCTAssertEqual(link!["meta"], URL(string: "meta.rdf")!)
    }

    func testLinkHeaderParsingDouble() {
        let link = HttpLinkHeader.parse(headerValue: "<meta.rdf>;rel=meta,<http://www.google.com>;rel=link")

        XCTAssertNotNil(link)
        XCTAssertEqual(link!.keys.count, 2)
        XCTAssertEqual(link!["meta"], URL(string: "meta.rdf")!)
        XCTAssertEqual(link!["link"], URL(string: "http://www.google.com")!)
    }

    func testLinkHeaderParsingGitHubHeader() {
        let header = #"<https://api.github.com/events?page=2>; rel="next", <https://api.github.com/events?page=10>; rel="last"#
        let link = HttpLinkHeader.parse(headerValue: header)

        XCTAssertNotNil(link)
        XCTAssertEqual(link!.keys.count, 2)
        XCTAssertEqual(link!["next"], URL(string: "https://api.github.com/events?page=2")!)
        XCTAssertEqual(link!["last"], URL(string: "https://api.github.com/events?page=10")!)
    }
}
